// card class and deck class
//
// test programs 2 shuffle
//
#include "card57.h"
#include "deck57.h"

int main()
{
	deck d;
	char ch;
	while (!d.isEmpty())
		cout << d.draw() << endl;
	cout << endl << endl << "Press enter to continue.";
	d.refill();
	d.shuffle();
	while (!d.isEmpty())
		cout << d.draw() << endl;
}

	