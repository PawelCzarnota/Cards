//Class declaration file: rational.h

#ifndef RATIONAL_H

#include <iostream.h>
#include "bool.h"

class rational
{
	public:

	//Constructors

	rational();
	rational(int numerator, int denominator);
	rational(const rational &r);

	//Accessors

	int numerator() const
		{return my_numerator;};
	int denominator() const
		{return my_denominator;};

	//Assignment

 //	const rational& operator = (const rational &rhs)
  //	{};

	private:

	//Data members

	int my_numerator, my_denominator;

	//Utility function

	void reduce();
};

//Free functions that operate on rational numbers

//Arithmetic

rational operator + (const rational &lhs,
		const rational &rhs);
rational operator - (const rational &lhs,
		const rational &rhs);
rational operator * (const rational &lhs,
		const rational &rhs);
rational operator / (const rational &lhs,
		const rational &rhs);

//Comparison

bool operator == (const rational &lhs, const rational &rhs);

//Input and Output

istream& operator >> (istream &is, rational &r);
ostream& operator << (ostream &os, const rational &r);

#define RATIONAL_H
#endif