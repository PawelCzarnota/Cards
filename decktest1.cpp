// card class and deck class
//
// test programs 1
//
#include "card57.h"
#include "deck57.h"

void main()
{
	deck d;
	char ch;
	while (!d.isEmpty())
		cout << d.draw() << endl;
	cout << endl << endl << "Press enter to continue.";
	cin.get(ch);
	d.refill();
	while (!d.isEmpty())
		cout << d.draw() << endl;
	cout << endl << endl << "Press enter to continue.";
}
	