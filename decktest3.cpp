// card class and deck class
//
// test programs 2 shuffle
//

#include "card57.h"
#include "deck57.h"
#include "apvector.h"

int main()
{
	deck d;
	int heartcount, acecount, i;
	apvector<card> hand(52);

	for (i = 0; i < 52; ++i)
	{
		hand[i] = d.draw();
		cout << hand[i] << endl;
	}

	heartcount = 0;
	for (i = 0; i < hand.length(); ++i)
	{
		if (hand[i].getSuit()== 'H')
			heartcount ++;
	}
	cout << "Number of hearts: " << heartcount << endl;

	acecount = 0;
	for (i=0; i < hand.length(); ++i)
	{
		if (hand[i].getRank() == 1)
			acecount ++;
	}

	cout << "Number of Aces: " << acecount << endl;

	return 0;
}

