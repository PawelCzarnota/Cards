#ifndef DECK_H
#define DECK_H

#include "apvector.h"
#include "card.h"
#include <time.h>
#include <stdlib.h>
#include <math.h>

class deck{

public:
	deck(); 					// construct full deck
	
	card draw(); 				// takes top card off deck
	
	void shuffle();				// scrambles deck
	void refill();				// resets deck to standard
	
	bool isEmpty() const; 
	bool isFull() const;
	
private:
	apvector<card> myCards;		// holds all cards
	int top;					// position in vector
};

void seed()
{
	time_t s;
	time(&s);
	srand((unsigned int) s);
}
	

deck::deck()
{
	myCards.resize(52);
	refill();
}

void deck::refill()				// resets deck to standard
{
	top = 0;
	char suit;	
	int rank;					// number on card
	int i=0;					// position in vector
	suit = 'C';
	for (rank = 1; rank <= 13; ++rank)\
	{
		myCards[i].setRank(rank);
		myCards[i].setSuit(suit);
		i++;
	}
	suit = 'D';	
	for (rank = 1; rank <= 13; ++rank)\
	{
		myCards[i].setRank(rank);
		myCards[i].setSuit(suit);
		i++;
	}
	suit = 'H';
	for (rank = 1; rank <= 13; ++rank)\
	{
		myCards[i].setRank(rank);
		myCards[i].setSuit(suit);
		i++;
	}
	suit = 'S';
	for (rank = 1; rank <= 13; ++rank)\
	{
		myCards[i].setRank(rank);
		myCards[i].setSuit(suit);
		i++;
	}
}
card deck::draw()
{
	if (isEmpty() )
		cout << "Error : draw from empty deck.";
	else
	{
		card topcard = myCards[top];
		top++;
		return topcard;
	}
}

void deck::shuffle()
{
	seed();
	int slot, pos;
for (int i = 0; i <=5; ++i)
{
	for (slot = 52; slot >1; --slot)
	{
		pos = rand()% slot;
		card temp= myCards[pos];
		myCards[pos] = myCards[slot - 1];
		myCards[slot -1] = temp;
	}		
}

}

bool deck::isEmpty() const
{
	return (top == 52);
}
bool deck::isFull() const
{
	if (top == 0) return true;
	else return false;
}

#endif 